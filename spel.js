//display name of player on screen
const playerName = localStorage.getItem("Player").replace(/"/g, "");
const contestant = document.createElement("h3");
const pName = document.createTextNode("プレイヤー: " + playerName);
contestant.appendChild(pName);

const playerElement = document.getElementById("playername");
playerElement.appendChild(contestant);

// players RPS selection
const playerImg = document.getElementById("playerRps");

const rock = document.getElementById("rock");
rock.addEventListener("click", () => {
  playerImg.src = "/image/rock.png";
  let tempResult = RPS(rock, getRandomInt());
  if (tempResult == "win") {
    result = "プレイヤーの勝ち";
  } else if (tempResult == "lose") {
    result = "プレイヤーの負け";
  } else if (tempResult == "draw") {
    result = "あいこ";
  }
  showResult();
});

const scissor = document.getElementById("scissor");
scissor.addEventListener("click", () => {
  playerImg.src = "/image/scissor.png";
  // RPS(scissor, getRandomInt());
  let tempResult = RPS(scissor, getRandomInt());
  if (tempResult == "win") {
    result = "プレイヤーの勝ち";
  } else if (tempResult == "lose") {
    result = "プレイヤーの負け";
  } else if (tempResult == "draw") {
    result = "あいこ";
  }
  showResult();
});

const paper = document.getElementById("paper");
paper.addEventListener("click", () => {
  playerImg.src = "/image/paper.png";
  let tempResult = RPS(paper, getRandomInt());
  if (tempResult == "win") {
    result = "プレイヤーの勝ち";
  } else if (tempResult == "lose") {
    result = "プレイヤーの負け";
  } else if (tempResult == "draw") {
    result = "あいこ";
  }
  showResult();
});

// players RPS selection
const cpuImg = document.getElementById("cpuRps");
//RPS
const RPS = (player, computer) => {
  if ((player == rock && computer == scissor) || (player == scissor && computer == paper) || (player == paper && computer == rock)) {
    return "win";
  } else if (player === computer) {
    return "draw";
  } else {
    return "lose";
  }
};

//rng for computer
function getRandomInt() {
  const cpu = Math.floor(Math.random() * 3);
  if (cpu == 0) {
    cpuImg.src = "/image/rock.png";
    return rock;
  } else if (cpu == 1) {
    cpuImg.src = "/image/scissor.png";
    return scissor;
  } else if (cpu == 2) {
    cpuImg.src = "/image/paper.png";
    return paper;
  }
}

//winner/loser annoucment
let result;
function showResult(){
  document.getElementById('repalced').innerText = result;
  let scoreboard = result;
  const textbox = document.createElement("li");
  const resulttext = document.createTextNode(scoreboard);
  textbox.appendChild(resulttext);
  let showWinner = document.getElementById("score");
  showWinner.appendChild(textbox);
}